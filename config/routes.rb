Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "home#index"

  resources :books, only: [:show, :index], param: :slug do
    resources :pages, only: :show, param: :number, format: true
  end
end
