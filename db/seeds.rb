# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

books = [
  {
    title: "Stories of Intellect",
    authors: ["E. Bulwer Lytton", "Harriet Prescott Spofford",
              "Edgar Allan Poe", "Nathaniel Hawthorne",
              "Thomas De Quincey", "Rebecca Harding Davis"],
    filename: "#{Rails.root}/db/data/61668-0.txt",
    line_separator: "\n"
  },
  {
    title: "A Tale of Two Cities",
    authors: ["Charles Dickens"],
    filename: "#{Rails.root}/db/data/98-0.txt"
  },
  {
    title: "Alice's Adventures in Wonderland",
    authors: ["Lewis Carroll"],
    filename: "#{Rails.root}/db/data/11-0.txt"
  },
  {
    title: "Frankenstein",
    authors: ["Mary Wollstonecraft (Godwin) Shelley"],
    filename: "#{Rails.root}/db/data/84-0.txt"
  },
  {
    title: "Ulysses",
    authors: ["James Joyce"],
    filename: "#{Rails.root}/db/data/4300-0.txt"
  },
  {
    title: "The Wonderful Wizard of Oz",
    authors: ["L. Frank Baum"],
    filename: "#{Rails.root}/db/data/55.txt.utf-8"
  }

]

books.each do |data|
  Book.new(title: data[:title], authors: data[:authors]).tap do |book|
    book_pages = GutenbergPageExtractor.new(File.open(data[:filename]))
      .extract(lines_per_page: 50)
    book_pages.each_with_index do |page, index|
      book.pages << BookPage.new(content: page, number: index + 1)
    end
  end.save!
end

puts "There are #{Book.all.count} books and #{BookPage.all.count} book pages"
