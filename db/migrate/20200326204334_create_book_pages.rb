class CreateBookPages < ActiveRecord::Migration[6.0]
  def change
    create_table :book_pages do |t|
      t.integer :number, null: false
      t.text :content, null: false
      t.references :book, null: false

      t.timestamps
    end

    add_index :book_pages, [:number, :book_id], unique: true
  end
end
