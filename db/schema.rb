# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_26_204334) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "book_pages", force: :cascade do |t|
    t.integer "number", null: false
    t.text "content", null: false
    t.bigint "book_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["book_id"], name: "index_book_pages_on_book_id"
    t.index ["number", "book_id"], name: "index_book_pages_on_number_and_book_id", unique: true
  end

  create_table "books", force: :cascade do |t|
    t.string "title", null: false
    t.text "authors", default: [], null: false, array: true
    t.string "slug", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slug"], name: "index_books_on_slug", unique: true
  end

end
