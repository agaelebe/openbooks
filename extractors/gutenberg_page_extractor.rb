class GutenbergPageExtractor
  START_SEPARATOR = %r{(\*\*\* START.+\*\*\*)}.freeze
  END_SEPARATOR = %r{(\*\*\* END.+\*\*\*)}.freeze

  def initialize(file)
    @file = file
  end

  # Returns and Array of pages with the contents and discarts metadata
  def extract(line_separator: "\n", lines_per_page: 50)
    _start, _separator, rest = @file.read.split(START_SEPARATOR)
    content, _separator, _end = rest.to_s.split(END_SEPARATOR)

    content.to_s
      .split(line_separator)
      .each_slice(lines_per_page)
      .to_a
      .map { |line| line.join("\n") }
      .reject { |c| c.empty? }
  end
end
