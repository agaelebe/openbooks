# README

The OpenBooks MVP app can:
 - Import new books from Gutemberg.org txt files
 - Display the latest books
 - Display list of all books
 - Display book pages in html or txt

This README documents some steps necessary to get the
application up and running.

Things you may want to cover:

## Ruby, Rails and Database versions

Ruby 2.6.6 / Rails 6.0 / PostgreSQL 9.6 (or higher)

## Install system dependencies

> bundle install

## Configuration

### Database creation

> rails db:create

### Database initialization

> rails db:migrate

### Database seed (add 6 books from db/data folder)

> rails db:seed

### How to run the test suite

> rspec

### Run server in dev mode

> rails s
