require "rails_helper"

describe "Page", type: :request do
  describe "GET books/:slug/page/:number" do
    before :all do
      book_a = Book.create!(title: "Book E", authors: ["Tracy"])
      BookPage.create!(content: "content of page 1", number: 1, book: book_a)
      BookPage.create!(content: "content of page 2", number: 2, book: book_a)
      
      book_b = Book.create!(title: "Book F", authors: ["John"])
      BookPage.create!(content: "this is the first page of the second book", number: 1, book: book_b)
      BookPage.create!(content: "this is the second page of the second book", number: 2, book: book_b)
      BookPage.create!(content: "this is the last page of the second book", number: 3, book: book_b)
    end

    context "html format" do
      it "shows the book title and the page content of book E" do
        get book_page_path(book_slug: "book-e", number: 1, format: "html")
        page = Nokogiri::HTML(response.body)

        expect(page.search(".book-page__title").first.text).to eq("Book E")
        expect(page.search(".book-page__content").first.text).to match("content of page 1")
        expect(page.search(".book-page__next a").first.to_s).to match('<a href=\"/books/book-e/pages/2.html\">next &gt;</a>')
      end

      it "shows content of page 2 of book E correctly" do
        get book_page_path(book_slug: "book-e", number: 2, format: "html")
        page = Nokogiri::HTML(response.body)

        expect(page.search(".book-page__title").first.text).to eq("Book E")
        expect(page.search(".book-page__content").first.text).to match("content of page 2")
        expect(page.search(".book-page__previous a").first.to_s).to match('<a href=\"/books/book-e/pages/1.html\">&lt; previous</a>')
      end

      it "shows content of page 2 of book F correctly" do
        get book_page_path(book_slug: "book-f", number: 2, format: "html")
        page = Nokogiri::HTML(response.body)

        expect(page.search(".book-page__title").first.text).to eq("Book F")
        expect(page.search(".book-page__content").first.text).to match("this is the second page of the second book")
      end

    end
    

    context "text format" do
      it "shows the book title, content and page number separated by hyphens" do
        get book_page_path(book_slug: "book-e", number: 1, format: "txt")

        expect(response.body).to eq(%(Book E
--------------------------------------------------------------------------------

content of page 1

--------------------------------------------------------------------------------
page 1 of 2
))
      end

      it "correclty shows the content of the page 2 of the bookk f" do
        get book_page_path(book_slug: "book-f", number: 2, format: "txt")

        expect(response.body).to eq(%(Book F
--------------------------------------------------------------------------------

this is the second page of the second book

--------------------------------------------------------------------------------
page 2 of 3
))
      end

    end
  end
end
