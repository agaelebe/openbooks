require "rails_helper"

describe "Home", type: :request do
  describe "GET /" do
    context "latest books" do
      it "shows the last 3 created books in the order: D, C, B" do
        Book.create!(title: "Book A", authors: ["John"])
        Book.create!(title: "Book B", authors: ["Jane"])
        Book.create!(title: "Book C", authors: ["Paul"])
        Book.create!(title: "Book D", authors: ["Ada"])

        get root_path

        page = Nokogiri::HTML(response.body)
        expect(page.search(".latest h3")[0].text).to eq("Book D")
        expect(page.search(".latest h3")[1].text).to eq("Book C")
        expect(page.search(".latest h3")[2].text).to eq("Book B")
      end
    end
  end
end
