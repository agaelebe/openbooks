require "rails_helper"

RSpec.describe GutenbergPageExtractor do
  let(:book_one) do
    File.new("#{Rails.root}/spec/fixtures/a_tale_of_two_cities_book_excerpt.txt")
  end

  describe "#extract" do
    subject do
      described_class.new(book_one).extract(line_separator: "\n", lines_per_page: 20)
    end

    it "extracts 18 pages" do
      expect(subject.size).to eq(18)
    end

    it "discards the metadata at the start" do
      book_start = %(




Produced by Judith Boss








A TALE OF TWO CITIES

A STORY OF THE FRENCH REVOLUTION

By Charles Dickens
)
      expect(subject.first).to eq(book_start)
    end

    it "discards the metadata at the end" do
      book_end = %{
“What is the matter?” asked the passenger, then, with mildly quavering
speech. “Who wants me? Is it Jerry?”

(“I don't like Jerry's voice, if it is Jerry,” growled the guard to
himself. “He's hoarser than suits me, is Jerry.”)




End of the Project Gutenberg EBook of A Tale of Two Cities, by Charles Dickens}

      expect(subject.last).to eq(book_end)
    end
  end
end
