require "rails_helper"

RSpec.describe BookPage, type: :model do
  describe "associations" do
    subject { described_class.new }
    it { is_expected.to belong_to(:book) }
  end

  context "validations" do
    subject {
      described_class.new(content: "Test", number: 1,
                          book: Book.new(title: "Bla", authors: ["Blo"]))
    }

    it { is_expected.to validate_presence_of :content }
    it { is_expected.to validate_numericality_of :number }
    it { is_expected.to validate_uniqueness_of(:number).scoped_to(:book_id) }
  end
end
