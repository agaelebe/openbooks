require "rails_helper"

RSpec.describe Book, type: :model do
  describe "associations" do
    subject { described_class.new }
    it { is_expected.to have_many(:pages).class_name("BookPage") }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of :title }
    it { is_expected.to validate_presence_of :authors }
    it { is_expected.to validate_presence_of :slug }
  end

  context "before validation callback" do
    it "creates a dasherized slug based on the title" do
      book = described_class.new(title: "Divine Comedy, Longfellow's Translation, Hell", authors: ["Dante Aligieri"])
      book.save!
      expect(book.slug).to eq("divine-comedy-longfellow-s-translation-hell")
    end

    it "adds a number starting in 2 to the end of the slug in case the slug already exists" do
      book = described_class.create!(title: "The Life and Most Surprising Adventures of Robinson Crusoe", authors: ["Dante Aligieri"])
      book_two = described_class.create!(title: "The Life and Most Surprising Adventures of Robinson Crusoe", authors: ["Dante Aligieri"])
      book_three = described_class.create!(title: "The Life and Most Surprising Adventures of Robinson Crusoe", authors: ["Dante Aligieri"])

      expect(book.slug).to eq("the-life-and-most-surprising-adventures-of-robinson-crusoe")
      expect(book_two.slug).to eq("the-life-and-most-surprising-adventures-of-robinson-crusoe-2")
      expect(book_three.slug).to eq("the-life-and-most-surprising-adventures-of-robinson-crusoe-3")
    end
  end

end
