class PagesController < ApplicationController
  def show
    @book = Book.find_by!(slug: params[:book_slug])
    @page = @book.pages.find_by!(number: params[:number])

    @total_pages = @book.pages.count

    respond_to(:html, :text)
  end
end
