class BooksController < ApplicationController
  def index
    @books = Book.ordered_by_title
  end

  def show
    book = Book.find_by!(slug: params[:slug])

    redirect_to book_page_path(book_slug: book.slug, number: 1, format: :html)
  end
end
