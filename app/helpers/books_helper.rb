module BooksHelper
  def display_authors(authors)
    case authors.size
    when 0
      "(unknown author)"
    when 1
      "by #{authors.first}"
    when 2
      "by #{authors.first} and #{authors.last}"
    else
      "by #{authors[0...-1].join(", ")} and #{authors.last}"
    end
  end
end
