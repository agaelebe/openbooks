class BookPage < ApplicationRecord
  belongs_to :book

  validates :content, presence: true
  validates :number, numericality: true
  validates_uniqueness_of :number, scope: [:book_id]
end
