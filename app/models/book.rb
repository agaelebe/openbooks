class Book < ApplicationRecord
  has_many :pages, class_name: "BookPage"

  before_validation :build_slug

  validates :title, :authors, :slug, presence: true
  validates :slug, uniqueness: true, on: :save

  scope :latest_books, -> { order("created_at DESC").limit(3) }

  scope :ordered_by_title, -> { order("title ASC") }

  private

  def build_slug
    slug = title.to_s.parameterize
    possible_slug = slug
    suffix = 2
    until self.class.find_by(slug: possible_slug).nil?
      possible_slug = "#{slug}-#{suffix}"
      suffix += 1
    end
    self.slug = possible_slug
  end
end
